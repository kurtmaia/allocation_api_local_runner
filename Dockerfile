FROM python:3.8-slim-buster

RUN apt update && \
	apt install -y git

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN python -m pip install git+https://gitlab.com/kurtmaia/allocation_api_local_runner.git

ENTRYPOINT ["start-api"]
