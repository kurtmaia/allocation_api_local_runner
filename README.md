# Allocation API local runner


## Getting started

1. Clone the repo
```bash
git clone git@gitlab.com:kurtmaia/allocation_api_local_runner.git
```
or
```bash
git clone https://gitlab.com/kurtmaia/allocation_api_local_runner.git
```

2. Install requirements
```bash
pip install -r requirements.txt
```

3. Install `allocation_api_local_runner` package
```bash
python -m build
pip install dist/allocation_api_local_runner-0.1.1-py3-none-any.whl
```
