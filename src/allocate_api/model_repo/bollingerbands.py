import numpy as np
import pandas as pd
import os, json,sys

def bollingerbands(df_in, window=20, sigma=2):
    df = df_in.copy()

    # measurement
    std = df['close'].rolling(window).std()
    sma = df['close'].rolling(window).mean()

    # indicator
    df['upper_bb'] = sma + std * sigma
    df['lower_bb'] = sma - std * sigma

    # signal
    df['position']  = 0
    
    # open position
    df['position'] = np.where(df['close'].shift(1) < df['lower_bb'].shift(1), 1, 0) 
    df['position'] = np.where(df['close'].shift(1) > df['upper_bb'].shift(1), -1, df['position'])
    
    # close position (next candle)
    df['position'] = np.where((df['position'].shift(1) == 1) | (df['position'].shift(1) == -1), 0, df['position'])
    return df['position'].map(float).values[-1]


def main():
    df = pd.read_csv(config['data_path']).set_index("symbol")
    allocation = {}
    n = 0
    for sym in list(set(df.index)):
        position = bollingerbands(df.loc[sym,])
        n += position
        allocation[sym] = position
    if n > 1:
        allocation = {k:v*1./n for k,v in allocation.items()}   
    json.dump(allocation, open(os.path.join(os.path.dirname(config['data_path']),"allocation.json"),'w'))

if __name__ == '__main__':
    config = json.load(open(sys.argv[1],'rb'))
    main()