from flask import Flask
from flask_restx import Api, Resource, fields


app = Flask(__name__)
api = Api(
    app, version="0.1.0", title="Investing api")

app.register_blueprint(allocate_bp, url_prefix='/allocate')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)