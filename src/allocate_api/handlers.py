import os, json, pathlib
import pandas as pd
import pickle as pk

import tempfile

from maiagomes.data.pull import get_alpha_vantage
import pandas as pd

def data_preper(assets):
	ans = []
	for sym in assets:
		try:
			tmp = get_alpha_vantage(symbol = sym, interval = '1min',outputsize=None).df[['Date','Close']]
			tmp.columns = [x.lower() for x in tmp.columns]
			tmp['symbol'] = sym
			ans.append(tmp)
		except Exception as err:
			print(f"Did not get data for {sym}: {err}")
	ans = pd.concat(ans,ignore_index = True)			
	ans.set_index("symbol", inplace = True)
	return ans

class get_allocation_from_strategy(object):
	def __init__(self, strategy = None, assets = None):
		filePath = os.path.dirname(__file__)
		model_config = json.load(open(os.path.join(filePath, "model_repo/model_config.json"),'rb'))
		self.model = strategy
		self.assets = assets.replace(' ','').split(',')
		self.tmp_dir = tempfile.TemporaryDirectory()			
		payload_path = os.path.join(self.tmp_dir.name,'payload.json')
		payload = {'model':strategy, 'assets': self.assets}
		cmd = model_config[strategy]['cmd']
		self.cmd = cmd.replace("<PAYLOAD_PATH>", payload_path).replace("<PATH>",f"'{filePath}'/model_repo")
		if model_config[strategy]['get_data']:
			data_path = os.path.join(self.tmp_dir.name,'data.csv')
			payload['data_path'] = data_path
			data_preper(self.assets).to_csv(data_path)
		json.dump(payload, open(payload_path,'w'))
	
	def allocate(self):
		os.system(self.cmd)
		allocation = json.load(open(os.path.join(self.tmp_dir.name,'allocation.json'),'rb'))
		self.tmp_dir.cleanup()
		return allocation



