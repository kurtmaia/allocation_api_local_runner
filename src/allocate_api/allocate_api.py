import json
import pickle as pk
from flask import Flask, request
from flask_restx import Api, Resource, fields
import os

from .handlers import get_allocation_from_strategy 

app = Flask(__name__)
api = Api(app, version="0.1.0", title="Investing api")

# define the main subroute for requests
ns = api.namespace("allocate", description="Allocate resources")

@ns.route("/")
class allocate(Resource):
    @ns.param("strategy", "Strategy")
    @ns.param("assets", "Assets")
    def get(self):
        W = get_allocation_from_strategy(strategy = request.args.get('strategy'), assets = request.args.get('assets')).allocate()
        return W

def main():
    app.run(host="0.0.0.0", port=3242)

if __name__ == '__main__':
    main()