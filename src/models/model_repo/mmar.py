import pickle as pk
import numpy as np
import pandas as pd
import os, pathlib

def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()

class mmar(object):
    def __init__(self):
        self.__getdata__ = False
    def allocate(self, historyPrices = None):
        return historyPrices

