import os, json, pathlib
import pandas as pd
import pickle as pk

from .model_repo import *

class get_allocation_from_strategy(object):
	def __init__(self, strategy = None, assets = None):
		try:
			self.model = eval(strategy)
		except Exception as err:
			print(err)
		self.assets = assets
		if self.model.__getdata__:
			self.df = get_data(assets)
		else:
			self.df = dict(zip(assets,[0]*len(assets)))			

	def allocate(self):
		return self.model.allocate(historyPrices = self.df)